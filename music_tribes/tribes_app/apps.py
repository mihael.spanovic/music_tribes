from django.apps import AppConfig


class TribesAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tribes_app'
